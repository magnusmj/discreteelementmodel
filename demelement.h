#ifndef DEMELEMENT_H
#define DEMELEMENT_H

#include <QObject>
#include <QPointF>
#include <QVector2D>

class DEMElement : public QObject
{
    Q_OBJECT
public:
    explicit DEMElement(QObject *parent = nullptr, QPointF position=QPointF(), QVector2D velocity=QVector2D());

    QPointF position;
    QVector2D velocity;
    QPointF dpos;
    float radius;
    float mass;
    float elasticity;


signals:

public slots:
};

#endif // DEMELEMENT_H
