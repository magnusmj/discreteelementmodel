#include "demsolver.h"

#include <QSize>
#include <QVector2D>

DEMSolver::DEMSolver(QObject *parent, QSizeF size) : QObject(parent)
{
    this->size = size;
}

void DEMSolver::solve(float deltaTime)
{
    for (int i = 0; i < this->elements.count(); i++){
        DEMElement *element = this->elements[i];
        for (int j = i+1; j < this->elements.count(); j++){
            DEMElement *otherElement = this->elements[j];
            float combined_radius = element->radius+otherElement->radius;
            QVector2D delta(otherElement->position - element->position);
            if (abs(delta[0]) < combined_radius and abs(delta[1]) < combined_radius){
                float dist = delta.length();
                delta.normalize();

                if (dist < combined_radius){
                    QVector2D vdir = otherElement->velocity.normalized() - element->velocity.normalized();
                    float imp = QVector2D::dotProduct(vdir, delta);
                    if (imp<0){
                        float p1 = QVector2D::dotProduct(element->velocity, delta) * element->mass;
                        float p2 = QVector2D::dotProduct(otherElement->velocity, delta) * otherElement->mass;
                        float combined_elasticity = element->elasticity*otherElement->elasticity;
                        element->velocity += (delta * p2 * combined_elasticity - delta * p1) / element->mass;
                        otherElement->velocity += (delta * p1 * combined_elasticity - delta * p2) / otherElement->mass;
                        element->position -= (combined_radius - dist) * delta.toPointF();
                        otherElement->position += (combined_radius - dist) * delta.toPointF();
                    } else {
                        float repulsion = 1.0f * (1 - element->elasticity * otherElement->elasticity);
                        element->velocity -= (combined_radius - dist) * delta * repulsion;
                        otherElement->velocity += (combined_radius - dist) * delta * repulsion;
                    }
                }
            }
        }

        element->position += element->velocity.toPointF() * deltaTime;
        element->velocity.setY(element->velocity.y()- this->gravity * deltaTime);

        if (element->position.x() < 0){                                             // Left side
            element->position.setX(0);
            element->velocity.setX(element->velocity.x() * element->elasticity);
            element->velocity.setX(abs(element->velocity.x()));
            if (this->worldMovement.x() > 0)                                        // If world moves, boundaries shall add
                element->velocity.setX(element->velocity.x() + this->worldMovement.x()*10); 		// some velocity to elements
        }
        if (element->position.x() > this->size.width()){                            // Right side
            element->position.setX(this->size.width());
            element->velocity.setX(element->velocity.x() * element->elasticity);
            element->velocity.setX(-abs(element->velocity.x()));
            if (this->worldMovement.x() < 0)                                        // If world moves, boundaries shall add
                element->velocity.setX(element->velocity.x() + this->worldMovement.x()*10); 		// some velocity to elements
        }
        if (element->position.y() < 0){                                             // Bottom
            element->position.setY(0);
            element->velocity.setY(element->velocity.y() * element->elasticity);
            element->velocity.setY(abs(element->velocity.y()));
            if (this->worldMovement.y() > 0)                                        // If world moves, boundaries shall add
                element->velocity.setY(element->velocity.y() + this->worldMovement.y()*10); 		// some velocity to elements
        }
    }
    this->worldMovement *= 0;
}

void DEMSolver::worldMoved(QPointF movement)
{
    this->worldMovement += movement;
    for (int i = 0; i < this->elements.count(); i++){
        DEMElement *element = this->elements[i];
        element->position -= movement;
    }
}
