#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "demwidget.h"

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void moveEvent(QMoveEvent *event);
private:
    DEMWidget *demWidget;
};

#endif // MAINWINDOW_H
