#ifndef DEMSOLVER_H
#define DEMSOLVER_H

#include "demelement.h"

#include <QObject>
#include <QSize>

class DEMSolver : public QObject
{
    Q_OBJECT
public:
    explicit DEMSolver(QObject *parent, QSizeF size);
    void solve(float delta);
    void worldMoved(QPointF movement);

    QList<DEMElement*> elements;
    float gravity = 982;
    QSizeF size;
    QPointF worldMovement;

signals:

public slots:
};

#endif // DEMSOLVER_H
