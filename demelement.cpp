#include "demelement.h"

DEMElement::DEMElement(QObject *parent, QPointF position, QVector2D velocity) : QObject(parent)
{
    this->position = position;
    this->velocity = velocity;
    this->radius = 20;
    this->elasticity = 0.7f;
    this->mass = 20;
    this->dpos = this->position * 1.0;
}
