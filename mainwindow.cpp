#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QMoveEvent>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent)
{
    this->demWidget = new DEMWidget(this);
    this->setCentralWidget(this->demWidget);
}

MainWindow::~MainWindow()
{

}

void MainWindow::moveEvent(QMoveEvent* event)
{
    QPointF delta = event->pos() - event->oldPos();
    this->demWidget->windowMoved(delta);
}
