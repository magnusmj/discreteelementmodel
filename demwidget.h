#ifndef DEMWIDGET_H
#define DEMWIDGET_H

#include "demsolver.h"
#include <QRandomGenerator>
#include <QTimer>
#include <QWidget>

class DEMWidget : public QWidget
{
    Q_OBJECT
public:
    explicit DEMWidget(QWidget *parent = nullptr);
    void windowMoved(QPointF delta);
    void paintEvent(QPaintEvent* qp);
    void mousePressEvent(QMouseEvent* qme);
    void resizeEvent(QResizeEvent *event);
signals:

public slots:
    void tick();
private:
    DEMSolver *demSolver;
    QTimer * timer;
    QRandomGenerator random;
};

#endif // DEMWIDGET_H
