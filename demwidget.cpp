#include "demwidget.h"

#include <QMoveEvent>
#include <QPainter>



DEMWidget::DEMWidget(QWidget *parent) : QWidget(parent)
{    
    this->demSolver = new DEMSolver(this, this->size());
    this->timer = new QTimer();
    //this->timer->timeout(this->tick);
    QObject::connect(this->timer, &QTimer::timeout, this, &DEMWidget::tick);
    this->timer->start(1000 / 60);
    this->setMouseTracking(true);
}

void DEMWidget::tick()
{
    // Update the physics
    //for (int i = 0; i < 6; i++)
    //    this->demSolver->solve(10.0 / 1000.0);
    this->demSolver->solve(60.0 / 1000.0);

    // Update the screen
    this->update();
}

void DEMWidget::windowMoved(QPointF delta)
{
    delta.setY(-delta.y());
    this->demSolver->worldMoved(delta);
}

void DEMWidget::paintEvent(QPaintEvent *qpe)
{
    // This function handles drawing of the widget
    // QPainter is the tool with which you draw
    QPainter qp(this);
    // Antialiasing makes stuff nicer.
    qp.setRenderHint(QPainter::Antialiasing);
    // We will draw our elements black
    qp.setBrush(Qt::black);
    // Go through each element in the solver and draw them.
    foreach (DEMElement *element, this->demSolver->elements){

        // Computer screens have the coordinate system upside down so upper left corner is origo and down is positive        # The rectangle describes the ellipse
        float r = element->radius;
        float d = 2*r;

        element->dpos += (element->position - element->dpos)*0.3;
        QRectF rect(element->dpos.x() - r, -element->dpos.y() - r + this->height(), d, d);
        // Here the ellipse is drawn
        qp.drawEllipse(rect);
    }

}

void DEMWidget::mousePressEvent(QMouseEvent *qme)
{
    // This function is executed when you click the widget with your mouse
    // Getting the mouse position from the event
    QPointF position = qme->pos();
    // Calculating the upside down input position into normal upside down
    position.setY(-position.y() + this->height());
    // Adding a new element to the solver
    DEMElement *element = new DEMElement(this, position);
    element->radius = 10 + this->random.bounded(30);
    element->mass = element->radius*element->radius;
    this->demSolver->elements.append(element);
}

void DEMWidget::resizeEvent(QResizeEvent *event)
{
    this->demSolver->size = this->size();
}


